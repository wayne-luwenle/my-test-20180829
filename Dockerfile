FROM tomcat
MAINTAINER wayne zhbitluwenle@163.com
EXPOSE 8080
ADD ./target/gitLabTest.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]