package wayne.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorld {
	@RequestMapping("/toIndex.do")
	public String toIndex() {
		return "index";
	}
}
